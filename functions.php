<?php

// Load CSS
add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );
function theme_enqueue_styles() {
    // Always load parent styles
    wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );

    // Load CSS and javascript for individual pages conditionally
    // if ( is_page( 'home' ) ) {    
        // home CSS file
        wp_enqueue_style('jut-home-css', get_stylesheet_directory_uri().'/css/jut-home.css', false ,'1.0', 'all' );

        // svg-to-path converter
        wp_enqueue_script( 'jut_svgmaker_JS', get_stylesheet_directory_uri() . '/js/jut-svgmaker.js', array ( 'jquery' ), 1.0, true);
        
        // particles.js library and custom JS
        wp_enqueue_script( 'jut_particles_JS', get_stylesheet_directory_uri() . '/js/lib/particles-js/particles.min.js', 1.0, true);
        wp_enqueue_script( 'jut_particles_bg_JS', get_stylesheet_directory_uri() . '/js/jut-particles-bg.js', array ( 'jut_particles_JS' ), 1.0, true);
    
        // scrollmagic.js and custom JS
        wp_enqueue_script( 'jut_scrollmagic_JS', get_stylesheet_directory_uri() . '/js/lib/scrollmagic-js/ScrollMagic.js', array ( 'jquery' ), 1.0, true);
        wp_enqueue_script( 'jut_tween_max_JS', get_stylesheet_directory_uri() . '/js/lib/greensock-js/TweenMax.min.js', array ( 'jquery' ), 1.0, true);
        wp_enqueue_script( 'jut_scrollmagic_JS_indicators', get_stylesheet_directory_uri() . '/js/lib/scrollmagic-js/plugins/debug.addIndicators.min.js', array ( 'jut_scrollmagic_JS' ), 1.0, true);
        wp_enqueue_script( 'jut_scrollmagic_JS_animations', get_stylesheet_directory_uri() . '/js/lib/scrollmagic-js/plugins/animation.gsap.min.js', array ( 'jut_scrollmagic_JS' ), 1.0, true);
        wp_enqueue_script( 'jut_scrollmagic_scenes_JS', get_stylesheet_directory_uri() . '/js/jut-scrollmagic-scenes.js', array ( 'jut_scrollmagic_JS' ), 1.0, true);

        // portfolio.js
        wp_enqueue_script( 'jut_home_portfolio_JS', get_stylesheet_directory_uri() . '/js/jut-home-portfolio.js', array ( 'jquery' ), 1.0, true);
    // } 

    if ( is_page( 'impressum' ) ) {
        wp_enqueue_style('jut-subpage-css', get_stylesheet_directory_uri().'/css/jut-subpage.css', false ,'1.0', 'all' );
    }

    if ( is_page( 'datenschutz' ) ) {
        wp_enqueue_style('jut-subpage-css', get_stylesheet_directory_uri().'/css/jut-subpage.css', false ,'1.0', 'all' );
    }

    if ( is_page( 'sales-deck' ) ) {
        wp_enqueue_style('jut-sales-deck-css', get_stylesheet_directory_uri().'/css/jut-sales-deck.css', false ,'1.0', 'all' );
        wp_enqueue_script( 'jut-z-scroll-js', get_stylesheet_directory_uri() . '/js/jut-z-scroll.js', array ( 'jquery' ), 1.0, true);
        wp_enqueue_script( 'jut_axios_JS', get_stylesheet_directory_uri() . '/js/lib/axios-master/dist/axios.min.js', array ( 'jquery' ), 1.0, true);
    }
}

// // Add particle background
// add_action( 'wp_head', 'jut_add_particle_bg', 999 );

// function jut_add_particle_bg() {
//     echo '<div id="particles-js"></div>';
// }

// Add lines background
add_action( 'wp_head', 'jut_add_lines_bg', 999 );

function jut_add_lines_bg() {
    echo    '<div class="lines">
                <div class="line"></div>
                <div class="line"></div>
                <div class="line"></div>
                <div class="line"></div>
                <div class="line"></div>
            </div>';
}

