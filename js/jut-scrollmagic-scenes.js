jQuery(document).ready(function($){

    // console.clear();

    var w = window.innerWidth;
    var tween = new TimelineMax();
    var controller;

    var size = w > 980 ? "big" : "small";

    // call function if window-size is "big"

    if (size === "big") {
        makeScrollMagic();
    }

    // window.onscroll = function (e) {
    //     console.log(window.scrollY); // Value of scroll Y in px
    // };

    // function to enable ScrollMagic

    function makeScrollMagic() {

        controller = new ScrollMagic.Controller();

        var logoMiddle = $("#jut-home-logo").offset().top + $("#jut-home-logo").height() / 2;

        // console.log(logoMiddle);

        var logoDuration = $("#jut-footer").offset().top - logoMiddle * 1.2;

        // console.log(logoDuration);

        // build scene to add pin to header logo
        var logoPinScene = new ScrollMagic.Scene({duration: logoDuration})
            .setPin("#jut-home-logo", {pushFollowers: false})
            // .addIndicators({name: "trigger"}) // add indicators (requires plugin)
            .addTo(controller);

        // Add scroll position dependent animation (tween)
        var tween = TweenMax.to("#jut-home-logo", 1, {className: "+=jut-logo-scrolled"});

        // Reduce logo size when scrolling down
        
        var logoSizeScene = new ScrollMagic.Scene({duration: 500})
            .setTween(tween)
            // .addIndicators({name: "tween css class"}) // add indicators (requires plugin)
            .addTo(controller);




        // Add movement to bubble
        var bubbleTween = TweenMax.to("#jut-home-bubble-spaeti", 1, {className: "+=jut-home-bubble-spaeti-scrolled"});

        var movingBubbleScene = new ScrollMagic.Scene({
            duration: 900,
            triggerElement: '#jut-home-section-spaeti',})
            .setTween(bubbleTween)
            // .addIndicators({name: "start bubble moving"})
            .addTo(controller);
    
        // Add size increase to bubble team
        var bubbleTeamTween = TweenMax.to("#jut-home-bubble-team", 1, {className: "+=jut-home-bubble-team-scale"});

        var movingBubbleTeamScene = new ScrollMagic.Scene({
            duration: 900,
            triggerElement: '#jut-home-section-team',})
            .setTween(bubbleTeamTween)
            // .addIndicators({name: "start bubble moving"})
            .addTo(controller);
    

        // Trigger animation of Späti sign based on scroll position
        var flickeringLogoScene = new ScrollMagic.Scene({
            triggerElement: '#jut-home-spaeti-aus',
            offset: 80})
            .setClassToggle('#jut-home-spaeti-an', 'jut-sign-activated')
            .reverse(false)
            // .addIndicators({name: 'add activated class'}) // add indicators
            .addTo(controller);


        // Flying money
        var flightpath = {
            entry : {
                curviness: 1.25,
                autoRotate: true,
                values: [
                        {x: 100,	y: -20},
                        {x: 300,	y: 10}
                    ]
            },
            looping : {
                curviness: 1.25,
                autoRotate: true,
                values: [
                        {x: 510,	y: 60},
                        {x: 620,	y: -60},
                        {x: 500,	y: -100},
                        {x: 380,	y: 20},
                        {x: 500,	y: 60},
                        {x: 580,	y: 20},
                        {x: 620,	y: 15}
                    ]
            },
            leave : {
                curviness: 1.25,
                autoRotate: true,
                values: [
                        {x: 660,	y: 20},
                        {x: 800,	y: 130},
                        {x: $(window).width(),	y: -100},
                    ]
            }
        };

        // create tween
        var moneyTween = new TimelineMax()
            .add(TweenMax.to($("#jut-money img"), 1.2, {css:{bezier:flightpath.entry}, ease:Power1.easeInOut}))
            .add(TweenMax.to($("#jut-money img"), 2, {css:{bezier:flightpath.looping}, ease:Power1.easeInOut}))
            .add(TweenMax.to($("#jut-money img"), 1, {css:{bezier:flightpath.leave}, ease:Power1.easeInOut}));

        // Build scene
        var scene = new ScrollMagic.Scene({triggerElement: "#jut-money-trigger", duration: 1000, triggerHook: 1})      
            .setTween(moneyTween)
            // .addIndicators({name: 'money trigger'}) // add indicators (requires plugin)
            .addTo(controller);

        // Fade animation of intro text block
        var fadeIntroScene = new ScrollMagic.Scene({offset: 50})
        .setClassToggle("#jut-home-intro-text .et_pb_text_inner", "jut-intro-text-fade")
        // .addIndicators({name: "1 - add a class"}) // add indicators (requires plugin)
        .addTo(controller);
    }

    var beethovenWinkScene = new ScrollMagic.Scene({
        triggerElement: '#jut-home-bubble-code',
        offset: 80,
        duration: 50})
        .setClassToggle('#jut-beethoven-wink', 'jut-beethoven-wink')
        .reverse(true)
        // .addIndicators({name: 'add activated class'}) // add indicators
        .addTo(controller);

    

    // function to check if screen is big enough for makeScrollMagic function to be triggered

    function sizeIt() {
    w = window.innerWidth;
    var newSize = w > 980 ? "big" : "small";
        if (newSize != size) {
            size = newSize;
            if (newSize === "small") {
            console.log("The screen is now small - ScrollMagic has been destroyed by aliens.");
            TweenMax.set("#jut-home-logo", { clearProps: "all" });
            tween.clear();
            controller.destroy(true);
            } else {
            console.log("The screen is now large - ScrollMagic is active.");
            makeScrollMagic();
            }
        }
    }

    // event listener for window resize

    window.addEventListener("resize", sizeIt);

    // Footer scroll up button

    function topFunction() {
        $('body,html').animate({
            scrollTop: 0
        }, 800);
        return false;
    }

    $('#jut-footer-scroll-up a').click(topFunction);
});