let films = [];

const perspectiveOrigin = {
    x: parseFloat(
      getComputedStyle(document.documentElement).getPropertyValue(
        "--scenePerspectiveOriginX"
      )
    ),
    y: parseFloat(
      getComputedStyle(document.documentElement).getPropertyValue(
        "--scenePerspectiveOriginY"
      )
    ),
    maxGap: 10
};

document.addEventListener("DOMContentLoaded", function() {
axios
    .get("#")
    .then(function(response) {
    films = response.data;
    window.addEventListener("scroll", moveCamera);
    window.addEventListener("mousemove", moveCameraAngle);
    setSceneHeight();
    })
    .catch(function(error) {
    console.log(error);
    });
});

function moveCameraAngle(event) {
    const xGap =
      (((event.clientX - window.innerWidth / 2) * 100) /
        (window.innerWidth / 2)) *
      -1;
    const yGap =
      (((event.clientY - window.innerHeight / 2) * 100) /
        (window.innerHeight / 2)) *
      -1;
    const newPerspectiveOriginX =
      perspectiveOrigin.x + (xGap * perspectiveOrigin.maxGap) / 100;
    const newPerspectiveOriginY =
      perspectiveOrigin.y + (yGap * perspectiveOrigin.maxGap) / 100;
  
    document.documentElement.style.setProperty(
      "--scenePerspectiveOriginX",
      newPerspectiveOriginX
    );
    document.documentElement.style.setProperty(
      "--scenePerspectiveOriginY",
      newPerspectiveOriginY
    );
}

function moveCamera() {
document.documentElement.style.setProperty("--cameraZ", window.pageYOffset);
}

function setSceneHeight() {
const numberOfItems = films.length; // Or number of items you have in `.scene3D`
const itemZ = parseFloat(
    getComputedStyle(document.documentElement).getPropertyValue("--itemZ")
);
const scenePerspective = parseFloat(
    getComputedStyle(document.documentElement).getPropertyValue(
    "--scenePerspective"
    )
);
const cameraSpeed = parseFloat(
    getComputedStyle(document.documentElement).getPropertyValue("--cameraSpeed")
);

const height =
    window.innerHeight +
    scenePerspective * cameraSpeed +
    itemZ * cameraSpeed * numberOfItems;

// Update --viewportHeight value
document.documentElement.style.setProperty("--viewportHeight", height);
}

jQuery(document).ready(function($){
  // Prüfen ob div die Klasse full-width-item hat, um dann den translate3d-Z-Wert entsprechend zu berechnen
  numberOfItems = $(".scene3D > div").length;

  for(i = 0; i < numberOfItems; i++) {
    if ($( ".scene3D > div:nth-child(" + i + ")" ).hasClass("full-width-item")){
      $(".scene3D > div:nth-child(" + i + ")").css({"transform": "translate3D(0,0,calc(8 * 150 *"+ i +"* -1px)"});
    }  
  }

  // Opacity Adjustments

  const itemZ = parseFloat(
    getComputedStyle(document.documentElement).getPropertyValue("--itemZ")
  );
 
  const cameraSpeed = parseFloat(
      getComputedStyle(document.documentElement).getPropertyValue("--cameraSpeed")
  );


  $(window).on('scroll', function () {

  // // loop

  var scrollTop = $(this).scrollTop(),
      gapBetweenItems = itemZ * cameraSpeed;

  for(i = 0; i <= numberOfItems; i++) {

      var distanceFromSceen = (gapBetweenItems * (i + 1)) - scrollTop;
      // console.log("Distanz vom Screen für item " + i + ": " + distanceFromSceen);

      var itemOpacity = (1.5 * gapBetweenItems) / distanceFromSceen;
      // console.log("Opacity für item " + i + ": " + itemOpacity);

      var item = $(".scene3D > div:nth-child(" + i + ")");
      item.css({ 'opacity': itemOpacity});

      if (itemOpacity > '1') {
        item.css({ 'opacity': 1 });
      } else if ( distanceFromSceen > 2 * gapBetweenItems ) {
        item.css({ 'opacity': 0 });
      }
  }
  });

  function topFunction() {
    $('body,html').animate({
        scrollTop: 0
    }, 2000);
    return false;
  }

  $('#jut-sales-scroll-back a').click(topFunction);
});
