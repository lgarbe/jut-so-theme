jQuery(document).ready(function($){

    $( "#jut-home-portfolio-slider" ).prepend( '<input type="radio" name="slider" id="s1">' );
    $( "#jut-home-portfolio-slider" ).prepend( '<input type="radio" name="slider" id="s2">' );
    $( "#jut-home-portfolio-slider" ).prepend( '<input type="radio" name="slider" id="s3" checked>' );
    $( "#jut-home-portfolio-slider" ).prepend( '<input type="radio" name="slider" id="s4">' );
    $( "#jut-home-portfolio-slider" ).prepend( '<input type="radio" name="slider" id="s5">' );

    $( "#jut-home-slider-content-1" ).wrap( "<span id='jut-home-portfolio-slide-span-1'></span>" );
    $( "#jut-home-portfolio-slide-span-1" ).wrap( "<label for='s1' id='slide1'></label>" );

    $( "#jut-home-slider-content-2" ).wrap( "<span id='jut-home-portfolio-slide-span-2'></span>" );
    $( "#jut-home-portfolio-slide-span-2" ).wrap( "<label for='s2' id='slide2'></label>" );

    $( "#jut-home-slider-content-3" ).wrap( "<span id='jut-home-portfolio-slide-span-3'></span>" );
    $( "#jut-home-portfolio-slide-span-3" ).wrap( "<label for='s3' id='slide3'></label>" );

    $( "#jut-home-slider-content-4" ).wrap( "<span id='jut-home-portfolio-slide-span-4'></span>" );
    $( "#jut-home-portfolio-slide-span-4" ).wrap( "<label for='s4' id='slide4'></label>" );

    $( "#jut-home-slider-content-5" ).wrap( "<span id='jut-home-portfolio-slide-span-5'></span>" );
    $( "#jut-home-portfolio-slide-span-5" ).wrap( "<label for='s5' id='slide5'></label>" );

});
